import Card from '../UI/Card';
import ExpanseCell from './ExpenseCell';
import "./Expenses.css"

const Expenses = () =>{
    const expenses = [{
        expenseDate:new Date("2021-10-12"),
        expenseTitle : "Car Insurance",
        expenseAmount : 240
      },
      {
        expenseDate:new Date("2021-09-10"),
        expenseTitle : "New TV",
        expenseAmount : 200
      },
      {
        expenseDate:new Date("2021-06-20"),
        expenseTitle : "Medical Bill",
        expenseAmount : 400
      },{
        expenseDate:new Date("2021-06-04"),
        expenseTitle : "Smartphone",
        expenseAmount : 900
      }];
      
    return (
        <Card className="expenses">
            <ExpanseCell title={expenses[0].expenseTitle} date={expenses[0].expenseDate} amount={expenses[0].expenseAmount}></ExpanseCell>
        </Card>
    );
}

export default Expenses