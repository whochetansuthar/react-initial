import Expenses from "./components/Expenses/Expenses";

const App = () => {
  return (
    <div>
      <Expenses></Expenses>
    </div>
  );
}

export default App;
